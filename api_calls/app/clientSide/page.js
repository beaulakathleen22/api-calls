"use client";
import Card from "@/components/common/Card";
import Link from "next/link";
import React, { useEffect, useState } from "react";

function ClientSide() {
  const [data, setData] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("https://fakestoreapi.com/products");
      const result = await response.json();
      setData(result);
    };
    fetchData();
  }, []);

  return (
    <div>
      <Link href={"/"}> Server Side Rendering</Link>
      <div className="d-flex flex-wrap">
        {data?.map((element) => (
          <div>
            <Card data={element} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default ClientSide;
