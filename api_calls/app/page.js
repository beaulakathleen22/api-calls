import Card from "@/components/common/Card";
import Link from "next/link";

async function getData() {
  const res = await fetch("https://fakestoreapi.com/products");
  return res.json();
}

export default async function Page() {
  const data = await getData();

  return (
    <div>
      <Link href={"/clientSide"}>Client Side Rendering</Link>
      <div className="d-flex flex-wrap">
        {data?.map((element) => (
          <div>
            <Card data={element} />
          </div>
        ))}
      </div>
    </div>
  );
}
