import React from "react";
import "../../style/home.css";
import Image from "next/image";

function Card({ data }) {
  return (
    <div className="card cardWrapper">
      <div className="card-body">
        <h4 className="bold d-flex justify-content-center align-content-center">
          {data?.title}
        </h4>
        <h5>${data?.price}</h5>
        <Image width={100} height={100} src={data?.image} alt="product" />
        <div className="card-text">{data?.description}</div>
        {data?.rating ? (
          <div className="rating">
            <div>Rateing : {data?.rating?.rate}</div>
            <div>Count : {data?.rating?.count}</div>
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default Card;
